# Demo of TPWD minimal header and footer

The header and footer code in this demo is for use by TPWD web application developers and third-party developers creating applications for TPWD.

Please Note:

- **Do not edit links in the header.** TPWD logos should direct users to the TPWD web site.
- **Do not add any navigation links to the header.** Global navigation for your application should appear just beneath the black bar.
- **The code provided here does not apply analytics tracking.** If you are creating an application that will be served from TPWD servers, Please contact the TPWD Web Team to ensure analytics tracking is properly applied to your webpage.


